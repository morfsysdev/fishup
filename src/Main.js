import React from 'react';
import * as firebase from 'firebase';
import { Router, Scene } from 'react-native-router-flux';
import { View, ScrollView, StyleSheet } from 'react-native';

import Content from './components/Content';
import Product from './components/Product';
import Cart from './components/Cart';
import Footer from './components/Footer';

class Main extends React.Component{
    componentWillMount(){
        var config = {
            apiKey: "AIzaSyAWhaD09SMAi8hAb3T5WiknebMvY2jF0To",
            authDomain: "thep-8178c.firebaseapp.com",
            databaseURL: "https://thep-8178c.firebaseio.com",
            projectId: "thep-8178c",
            storageBucket: "thep-8178c.appspot.com",
            messagingSenderId: "511700176801"
        };
        firebase.initializeApp(config);
    }
    render(){
        return (
            <View style={{flex: 1}}>
                <Router navigationBarStyle={{ backgroundColor: '#f3f3f3' }}>
                    <Scene key="root">
                        <Scene key="home" component={Content} title="FishUP"/>
                        <Scene key="product" component={Product} title="Product"/>
                        <Scene key="cart" component={Cart} title="Cart"/>
                    </Scene>
                </Router>
                <Footer />
            </View>
        )
    }
}


const styles = StyleSheet.create({
    s1: {
        backgroundColor: '#f2f2f2'
    }
})

export default Main;