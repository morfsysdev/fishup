import axios from 'axios';
import { GET_FISHLIST,CURRENT_SCREEN } from './types';

export const getFishList = () => async dispatch => {
    const res = await axios.get('http://www.morfsys.com/projects/fishup/data3.json');
    dispatch({type: GET_FISHLIST, payload: res.data});
}

