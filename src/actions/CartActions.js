import { ADD_TO_CART, UPDATE_CART, REMOVE_FROM_CART } from './types';

export const addToCart = (cartItem) => dispatch => {
    dispatch({type: ADD_TO_CART, payload: cartItem});
}

export const updateCart = (id, unit) => dispatch => {
    dispatch({type: UPDATE_CART, payload:{ id, unit }})
}

export const removeFromCart = (id) => dispatch => {
    dispatch({type: REMOVE_FROM_CART, payload: id});
}