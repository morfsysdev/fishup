import { CREATE_USER, LOGIN_USER, AUTH_USER, DEMO_TEST } from './types';
import * as firebase from 'firebase';

export const createUser = (email, password) => dispatch => {
    firebase.auth().createUserWithEmailAndPassword(email, password)
        .then(function(user){
            dispatch({type: CREATE_USER, payload: user.email});
        }, function(e){
            //console.log('Failed')
        })
}

export const loginUser = (email, password) => dispatch => {
    console.log('alala')
    firebase.auth().signInWithEmailAndPassword(email, password)
        .then(function(user){
            dispatch({type: LOGIN_USER, payload: user.email});
        }, function(e){
            //console.log('Cant log in')
        })
}


export const authUser = (email) => dispatch => {
    dispatch({type: AUTH_USER, payload: email});
}
