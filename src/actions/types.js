export const GET_FISHLIST = 'get_fishlist';
export const ADD_TO_CART = 'add_to_cart';
export const UPDATE_CART = 'update_cart';
export const REMOVE_FROM_CART = 'remove_from_cart';
export const CREATE_USER = 'create_user';
export const LOGIN_USER = 'login_user';
export const AUTH_USER = 'auth_user';