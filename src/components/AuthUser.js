import React from 'react';
import { View, Text, TextInput, TouchableOpacity, StyleSheet, Modal } from 'react-native';
import * as firebase from 'firebase';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { createUser, loginUser } from '../actions/UserActions';

class AuthUser extends React.Component{

    constructor(){
        super();
        this.state = {
            email: '',
            password: '',
            modalVisible: false,
        }
    }

    render(){
        return (
            <View style={styles.v1}>
                <View><Text style={styles.t1}>BIG LOGO</Text></View>
                <View>
                    <TextInput 
                        style={styles.tI1}
                        onChangeText={(text)=>this.setState({email: text})}
                        value={this.state.email}
                        placeholder="Email ID / Username"
                    />
                    <TextInput 
                        style={styles.tI1}
                        onChangeText={(text)=>this.setState({password: text})}
                        value={this.state.password}
                        placeholder="Password"
                    />
                    <TouchableOpacity 
                        style={styles.tO1}
                        onPress={()=>{this.props.loginUser(this.state.email, this.state.password);}}
                    >
                        <Text style={styles.t2}>Log In</Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <View style={styles.v2}>
                        <View style={styles.v3}></View>
                        <Text style={styles.t4}>OR</Text>
                        <View style={styles.v3}></View>
                    </View>
                    <Modal
                        animationType="slide"
                        transparent={false}
                        visible={this.state.modalVisible}
                        onRequestClose={() => {alert("Modal has been closed.")}}
                    >
                        <View style={styles.v4}>
                            <View><Text style={styles.t1}>BIG LOGO</Text></View>
                            <View style={{marginLeft: 10, marginRight: 10}}>
                                <TextInput 
                                    style={styles.tI1}
                                    onChangeText={(text)=>this.setState({newEmail: text})}
                                    value={this.state.newEmail}
                                    placeholder="Email ID / Username"
                                />
                                <TextInput 
                                    style={styles.tI1}
                                    onChangeText={(text)=>this.setState({newPassword: text})}
                                    value={this.state.newPassword}
                                    placeholder="Password"
                                />
                                <TouchableOpacity 
                                    style={styles.tO1}
                                    onPress={()=>this.props.createUser(this.state.newEmail, this.state.newPassword)}
                                >
                                    <Text style={styles.t2}>Create Account</Text>
                                </TouchableOpacity>
                            </View>
                            <View>
                                <TouchableOpacity 
                                    style={styles.tO1}
                                    onPress={()=>this.setState({modalVisible: false})}
                                >
                                    <Text style={styles.t2}>Already have an account?</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </Modal>
                    <TouchableOpacity
                        style={styles.tO2}
                        onPress={()=>this.setState({modalVisible: true})}
                    >
                        <Text style={styles.t3}>Sign Up Now</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    v1: {
        margin: 10,
    },
    t1: {
        fontSize: 25,
        fontWeight: 'bold',
        marginBottom: 10,
        textAlign: 'center'
    },
    tI1:{
        height: 40, 
        borderColor: 'gray', 
        borderWidth: 1,
        padding: 10,
        marginBottom: 10
    },
    tO1: {
        backgroundColor: 'green',
        padding: 10,
        alignItems: 'center'
    },
    t2: {
        color: '#fff',
    },
    tO2: {
        backgroundColor: '#fff',
        padding: 10,
        borderColor: 'green',
        borderWidth: 1,
        alignItems: 'center'
    },
    t3: {
        color: 'green',
    },
    v2: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: 20,
        marginBottom: 20
    },
    v3: {
        flex: 2,
        backgroundColor: '#ccc',
        width: null,
        height: 1,
    },
    t4: {
        flex: 1,
        textAlign: 'center',
        color: '#888',
        fontStyle: 'italic'
    },
    v4: {
        marginTop: 60,
        flex: 1,
        justifyContent: 'space-between'
    }
});

function mapDispatchToProps(dispatch){
    return bindActionCreators({
        createUser,
        loginUser
    }, dispatch)
}

export default connect(null, mapDispatchToProps)(AuthUser);