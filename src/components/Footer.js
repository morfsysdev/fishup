import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome';
import { connect } from 'react-redux';

class Footer extends React.Component{
    screenSearch(){
        //this.props.currentScreen('search');
    }
    screenUser(){
        //this.props.currentScreen('user');
    }
    screenNav(){
        //this.props.currentScreen('nav');
    }
    render(){
        return (
            <View style={styles.v1}>

                <TouchableOpacity
                    style={styles.v2}
                    onPress={()=>Actions.cart()}
                >
                    <Icon style={styles.vc1} name="shopping-cart" />
                    <View style={styles.v3}>
                        <Text style={styles.tcartItems}>{this.props.cart.length}</Text>
                    </View>
                </TouchableOpacity>

                <TouchableOpacity
                    onPress={()=>this.screenSearch()}
                >
                    <Icon style={styles.vc1} name="search" />
                </TouchableOpacity>

                <TouchableOpacity
                    onPress={()=>this.screenUser()}
                >
                    <Icon style={styles.vc1} name="user" />
                </TouchableOpacity>

                <TouchableOpacity
                onPress={()=>this.screenNav()}
                >
                    <Icon style={styles.vc1} name="navicon" />
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    v1:{
        flexDirection: 'row',
        justifyContent: 'space-around',
        backgroundColor: '#f8f8f8',
        borderTopWidth: 1,
        borderColor: '#ccc',
        shadowColor: "#000000",
        shadowOpacity: 0.2,
        shadowRadius: 2,
        shadowOffset: {
          height: 1,
          width: 0
        }
    },
    v2: {
        position: 'relative'
    },
    vc1:{
        fontSize: 20,
        padding: 15
    },
    v3: {
        position: 'absolute',
        right: 0,
        borderRadius: 50,
        paddingTop: 5,
        paddingBottom: 5,
        paddingLeft: 8,
        paddingRight: 8,
        borderWidth: 1,
        borderColor: 'red',
        backgroundColor: 'red'
    },
    tcartItems: {
        color: '#fff',
        fontSize: 10
    }
})

function mapStateToProps(state){
    return {
        cart: state.cart.cart
    };
}

export default connect(mapStateToProps)(Footer);