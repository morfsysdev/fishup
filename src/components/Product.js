import React from 'react';
import { View, Text, Image, TouchableOpacity, StyleSheet, ScrollView } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

class Product extends React.Component{
    constructor(){
        super();
        this.state = {
            currentTab: 'overview'
        }
    }
    renderTab(){
        if(this.state.currentTab=='nutrition'){
            return (
                <View>
                    <View style={styles.v4}><Text style={styles.tNut}>Nutritional Facts (per 100g)</Text></View>
                    <View style={styles.v4}>
                        <View style={styles.v5}>
                            <Text style={styles.tNutLabel}>Calories</Text>
                            <Text style={styles.tNutVal}>{this.props.calories}</Text>
                        </View>
                        <View style={styles.v5}>
                            <Text style={styles.tNutLabel}>Fat</Text>
                            <Text style={styles.tNutVal}>{this.props.fat}</Text>
                        </View>
                        <View style={styles.v5}>
                            <Text style={styles.tNutLabel}>Polyunsaturated</Text>
                            <Text style={styles.tNutVal}>{this.props.polyunsaturated}</Text>
                        </View>
                        <View style={styles.v5}>
                            <Text style={styles.tNutLabel}>Monounsaturated</Text>
                            <Text style={styles.tNutVal}>{this.props.monounsaturated}</Text>
                        </View>
                        <View style={styles.v5}>
                            <Text style={styles.tNutLabel}>Cholesterol</Text>
                            <Text style={styles.tNutVal}>{this.props.cholesterol}</Text>
                        </View>
                        <View style={styles.v5}>
                            <Text style={styles.tNutLabel}>Dietary Fibre</Text>
                            <Text style={styles.tNutVal}>{this.props.fibre}</Text>
                        </View>
                    </View>
                </View>
            )
        }else if(this.state.currentTab=='reviews'){
            return (
                <View style={styles.v6}>
                    <TouchableOpacity style={styles.tO1}>
                        <Text>Leave a review</Text>
                        <View><Icon name="chevron-circle-right" size={15} style={{color: '#ccc'}}/></View>
                    </TouchableOpacity>
                    <View style={styles.v7}>
                        <View style={styles.v8}>
                            <Text style={styles.t2}>Healthy and tasty</Text>
                            <Text style={styles.t3}>2 days ago</Text>
                        </View>
                        <View style={styles.v9}>
                            <Icon style={styles.starIcon} name="star"/>
                            <Icon style={styles.starIcon} name="star"/>
                            <Icon style={styles.starIcon} name="star"/>
                            <Icon style={styles.starIcon} name="star"/>
                            <Icon style={styles.starIcon} name="star-o"/>
                            <Text style={styles.t4}> by Aditya Singh</Text>
                        </View>
                        <Text style={styles.t5}>Lorem ipsum dolor sit ene bojus eskel mita rami elditso pila kima beto.</Text>
                    </View>
                    <View style={styles.v7}>
                        <View style={styles.v8}>
                            <Text style={styles.t2}>Healthy and tasty</Text>
                            <Text style={styles.t3}>2 days ago</Text>
                        </View>
                        <View style={styles.v9}>
                            <Icon style={styles.starIcon} name="star"/>
                            <Icon style={styles.starIcon} name="star"/>
                            <Icon style={styles.starIcon} name="star"/>
                            <Icon style={styles.starIcon} name="star"/>
                            <Icon style={styles.starIcon} name="star-o"/>
                            <Text style={styles.t4}> by Aditya Singh</Text>
                        </View>
                        <Text style={styles.t5}>Lorem ipsum dolor sit ene bojus eskel mita rami elditso pila kima beto.</Text>
                    </View>
                </View>
            )
        }else{
            return (
                <View>
                    <Text style={styles.bPrice}>Price: <Text style={styles.bPriceVal}><Icon name="rupee" size={15} /> {this.props.price}</Text></Text>
                    <Text>{this.props.desc}</Text>
                </View>
            )
        }
    }
    render(){
        return(
            <ScrollView style={styles.v1}>
                <View>
                    <Image 
                        source={{uri: this.props.uri}} 
                        style={{width:null, height: 200}}
                    />
                </View>
                <View style={styles.v2}>
                    <TouchableOpacity onPress={()=>this.setState({currentTab: 'overview'})}><Text style={styles.t1}>Overview</Text></TouchableOpacity>
                    <TouchableOpacity onPress={()=>this.setState({currentTab: 'nutrition'})}><Text style={styles.t1}>Nutrition</Text></TouchableOpacity>
                    <TouchableOpacity onPress={()=>this.setState({currentTab: 'reviews'})}><Text style={styles.t1}>Reviews</Text></TouchableOpacity>
                </View>
                <View style={styles.v3}>
                    {this.renderTab()}
                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    v1:{
        marginTop: 60,
        backgroundColor: '#f3f3f3',
        flex: 1
    },
    v2:{
        backgroundColor: 'green',
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    t1:{
        padding: 15,
        color: 'white',
        fontSize: 16
    },
    v3: {
        padding: 15,
    },
    bPrice: {
        fontSize: 16,
        paddingBottom: 15,
    },
    bPriceVal: {
        color: '#b12704'
    },
    v4:{
        borderBottomColor: '#333',
        borderBottomWidth: 2,
        paddingBottom: 10,
        marginBottom: 10 
    },
    tNut: {
        fontSize: 16,
        fontWeight: 'bold',
    },
    v5: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingBottom: 5,
    },
    tNutLabel: {
        fontWeight: 'bold' 
    },
    tO1:{
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        borderWidth: 2,
        borderColor: '#ccc',
        padding: 15,
        backgroundColor: '#f8f8f8',
        shadowColor: "#000000",
        shadowOpacity: 0.2,
        shadowRadius: 2,
        shadowOffset: {
          height: 1,
          width: 1
        }
    },
    v7: {
        paddingTop: 10,
        paddingBottom: 10,
        marginTop: 10,
        borderColor: '#ccc',
        borderWidth: 1,
        borderLeftWidth: 0,
        borderRightWidth: 0,
        borderTopWidth: 0
    },
    v8: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginBottom: 10,
    },
    t2: {
        fontWeight: 'bold',
        fontSize: 16
    },
    t3: {
        fontSize: 14,
        color: '#aaa'
    },
    v9: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 10,
    },
    starIcon: {
        fontSize: 12,
        color: '#F77D0E',
    },
    t5:{
        fontSize: 13,
        color: '#888'
    }
})

export default Product;