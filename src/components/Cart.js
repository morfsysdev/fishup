import React from 'react';
import { View, Text, ScrollView, StyleSheet, TouchableOpacity, Image, LayoutAnimation } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { removeFromCart, updateCart } from '../actions/CartActions';

class Cart extends React.Component{
    componentWillUpdate(){
        LayoutAnimation.spring();
    }
    renderReduceQty(q){
        if(parseInt(q.qty)>1){
            return (
                <TouchableOpacity 
                    style={styles.toUp}
                    onPress={()=>this.props.updateCart(q.id, -1)}
                >
                    <Icon name="minus" style={styles.tAdd} />
                </TouchableOpacity>
            )
        }else{
            return false;
        }
    }
    renderCart(){
        const cart = this.props.cart;
        if(cart.length>0){
            return cart.map((e)=>{
                const item = {
                    name: e.name,
                    uri: e.uri,
                    price: e.price,
                    qty: e.qty,
                };
                return (
                    <View key={e.id} style={styles.v1}>
                        <View style={styles.v2}>
                            <Image source={{uri: e.uri}} style={styles.iFish} />
                        </View>
                        <View style={styles.v3}>
                            <View style={styles.v4}>
                                <Text style={styles.t1}>{e.name}</Text>
                                <Text style={styles.t2}><Icon name="rupee" /> {e.price}</Text>
                            </View>
                            <View style={styles.v5}>
                                <View style={styles.v6}>
                                    {this.renderReduceQty(e)}
                                    <View style={styles.v8}><Text style={styles.tQty}>{e.qty}</Text></View>
                                    <TouchableOpacity 
                                        style={styles.toUp}
                                        onPress={()=>this.props.updateCart(e.id, +1)}
                                    >
                                        <Icon name="plus" style={styles.tAdd} />
                                    </TouchableOpacity>
                                </View>
                                <View>
                                    <TouchableOpacity style={styles.tO1} onPress={()=>this.props.removeFromCart(e.id)}><Text style={styles.t3}>Remove</Text></TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </View>
                )
            })
        }else{
            return (
                <Text>Empty Cart</Text>
            )
        }
    }
    render(){

        return(
            <ScrollView style={styles.sv}>
                {this.renderCart()}
            </ScrollView>
        )
    }
}



const styles = StyleSheet.create({
    sv:{
        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 70,
        backgroundColor: '#f3f3f3'
    },
    v1: {
        flexDirection: 'row',
        borderBottomWidth: 2,
        justifyContent: 'flex-start',
        borderColor: '#ccc',
        paddingBottom: 10,
        marginBottom: 10
    },
    v2: {
        marginRight: 10,
    },
    iFish: {
        width: 75,
        height: 75
    },
    v3: {
        flex: 1,
        justifyContent: 'flex-start',
        
    },
    v4: {
        width: null,
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 10,
    },
    t1: {
        fontWeight: 'bold',
    },
    t2: {
        color: '#b12704',
    },
    v5: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    tO1: {
        backgroundColor: 'green',
        padding: 5,
    },
    t3: {
        color: '#fff',
        fontSize: 12
    },
    v6: {
        flexDirection: 'row',
        justifyContent: 'flex-start'
    },
    tAdd: {
        color: '#fff',
        textAlign: 'center'
    },
    toUp:{
        padding: 10,
        backgroundColor: '#f0c14b',
        borderRadius: 50,
    },
    v8: {
        padding: 10
    },
    tQty: {
        color: '#333'
    }
});

function mapStateToProps(state){
    return {
        cart: state.cart.cart
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({
        removeFromCart,
        updateCart
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Cart);