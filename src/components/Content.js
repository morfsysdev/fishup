import React from 'react';
import { Actions } from 'react-native-router-flux';
import * as firebase from 'firebase';
import { View, Text, Image, StyleSheet, Button, TouchableOpacity, ScrollView } from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Icon from 'react-native-vector-icons/FontAwesome';

import { addToCart, updateCart } from '../actions/CartActions';
import { authUser } from '../actions/UserActions';
import { getFishList } from '../actions/FishActions';

import AuthUser from './AuthUser';

class Content extends React.Component{
    componentWillMount(){

        if(this.props.fishList.length>0){
            return;
        }else{
            this.props.getFishList();
        }
    }
    constructor(){
        super();
        this.state = {
            isLoggedIn: true
        }
    }
    addToCart(item){
        this.props.addToCart(item);
    }
    decrementQty(item){
        this.props.updateCart(item, -1);
    }
    incrementQty(item){
        this.props.updateCart(item, 1);
    }
    getQuantity(item){
        var qty=1;

        for(var i=0;i<this.props.cart.length;i++){
            if(item.id==this.props.cart[i].id){
                qty=this.props.cart[i].qty;
                break;
            }
        }
        return qty;
    }
    addToCartButton(item){
        var exi=false;
        for(var i=0;i<this.props.cart.length;i++){
            if(item.id==this.props.cart[i].id){
                exi=true;
                break;
            }
        }
        if(!exi){
            return (
                <View style={styles.v6}>
                    <TouchableOpacity style={styles.toAdd} onPress={()=> this.addToCart(item)} >
                        <Text style={styles.tAdd}>Add to cart</Text>
                    </TouchableOpacity>
                </View>
            )
        }else{
            return (
                <View style={styles.v7}>
                    <TouchableOpacity 
                        style={styles.toUp}
                        onPress={()=>this.decrementQty(item.id)}
                    >
                        <Text style={styles.tAdd}><Icon name="minus" size={15} /></Text>
                    </TouchableOpacity>
                    <View style={styles.v8}><Text style={styles.tQty}>{this.getQuantity(item)}</Text></View>
                    <TouchableOpacity 
                        style={styles.toUp}
                        onPress={()=>this.incrementQty(item.id)}
                    >
                        <Text style={styles.tAdd}><Icon name="plus" size={15} /></Text>
                    </TouchableOpacity>
                </View>
            )
        }
    }
    
    renderContent(){
        var that = this;
        firebase.auth().onAuthStateChanged(function(user){
            if(user){
                that.props.authUser(user.email);
            }else{
            }
        })
        const user = this.props.user;
        if(user.length>0){
            const fishes = this.props.fishList;
            if(fishes.length>0){
                return fishes.map((e)=>{
                    
                    const item = {
                        id: e.id,
                        name: e.name,
                        desc: e.desc,
                        uri: e.img,
                        price: e.price,
                        calories: e.nutrient.calories,
                        fat: e.nutrient.fat,
                        polyunsaturated: e.nutrient.Polyunsaturated,
                        monounsaturated: e.nutrient.Monounsaturated,
                        cholesterol: e.nutrient.cholesterol,
                        fibre: e.nutrient.fibre,
                        qty: 1
                    };
                    return (
                        <View key={e.id} style={styles.v1}>
                            <View style={styles.v2}>
                                <Image 
                                source={{uri: e.img}} 
                                style={styles.iFish}
                                />
                                <TouchableOpacity style={styles.overlay} onPress={()=>Actions.product(item)}></TouchableOpacity>
                                <View style={styles.v3}><Text style={styles.tRating}><Icon name="star" /> 4.8</Text></View>
                            </View>
                            <View style={styles.v4}><Text style={styles.tName}>{e.name}</Text></View>
                            <View style={styles.v5}><Text style={styles.bPrice}>Price: <Text style={styles.bPriceVal}><Icon name="rupee" size={15} /> {e.price}</Text></Text></View>      
                            {this.addToCartButton(item)}
                        </View>
                    );
                })
            }else{
                return <Spinner visible={true} />;
            }
        }else{
            return <AuthUser />
        }
    }
    render(){
        return (
            <ScrollView style={styles.sv}>
                {this.renderContent()}
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    sv:{
        marginTop: 60,
        backgroundColor: '#f3f3f3'
    },
    v1:{
        backgroundColor: '#fff',
        margin: 10,
        borderColor: '#ccc',
        borderWidth: 1,
        shadowColor: "#000000",
        shadowOpacity: 0.2,
        shadowRadius: 2,
        shadowOffset: {
          height: 1,
          width: 1
        }
    },
    v2: {
        position: 'relative',
    },
    overlay: {
        position: 'absolute',
        backgroundColor: '#000',
        opacity: .4,
        width: '100%',
        height: 200,
    },
    iFish: {
        width: null,
        height: 200
    },
    v3: {
        position: 'absolute',
        backgroundColor: '#4CAF50',
        opacity: .8,
        top: 10,
        right: 10,
        borderRadius: 4
    },
    tRating: {
        color: '#fff',
        padding: 5,
        fontSize: 12,
    },
    v4:{
        margin: 10
    },
    tName: {
        fontSize: 22,
        color: '#333'
    },
    v5: {
        marginBottom: 10,
        marginLeft: 10,
        marginRight: 10
    },
    bPrice: {
        fontSize: 16,
    },
    bPriceVal: {
        color: '#b12704'
    },
    buyNow: {
        paddingBottom: 10,
        paddingLeft: 10,
        paddingRight: 10,
    },
    v6: {
        margin: 10,
        marginTop: 0
    },
    toAdd:{
        padding: 10,
        backgroundColor: '#f0c14b',
        borderColor: '#a88734',
    },
    tAdd: {
        color: '#fff',
        textAlign: 'center'
    },
    v7: {
        flexDirection: 'row',
        justifyContent: 'center',
        margin: 10,
        marginTop: 0
    },
    toUp:{
        padding: 10,
        backgroundColor: '#f0c14b',
        borderRadius: 50,
    },
    v8: {
        backgroundColor: '#fff',
        padding: 10
    },
    tQty: {
        color: '#333'
    }
})

function mapStateToProps(state){
    return {
        fishList: state.fishList.fishList,
        cart: state.cart.cart,
        user: state.user.user,
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({
        getFishList,
        addToCart,
        updateCart,
        authUser

    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Content);