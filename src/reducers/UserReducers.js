import { CREATE_USER, LOGIN_USER, AUTH_USER } from '../actions/types';

export default function userReducers(state={user:[]}, action){
    switch(action.type){
        case CREATE_USER:
            return { user: action.payload };

        case LOGIN_USER:
            return { user: action.payload };

        case AUTH_USER:
            return { user: action.payload };

        default:
            return state;
    }
}