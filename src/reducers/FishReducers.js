import { GET_FISHLIST } from '../actions/types';

export default function fishReducers(state={fishList:[]}, action){
    switch(action.type){
        case GET_FISHLIST:
            return { fishList: action.payload };

        default:
            return state;
    }
}