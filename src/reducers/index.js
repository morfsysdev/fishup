import { combineReducers } from 'redux';
import fishReducers from './FishReducers';
import cartReducers from './CartReducers';
import userReducers from './UserReducers';

export default combineReducers({
    fishList: fishReducers,
    cart: cartReducers,
    user: userReducers
});