import { ADD_TO_CART, UPDATE_CART, REMOVE_FROM_CART } from '../actions/types';

export default function cartReducers(state={cart:[]}, action){
    switch(action.type){
        case ADD_TO_CART:
            if(state.cart==''){
                //if cart is empty
                return {cart: state.cart.concat(action.payload)};
            }else{
                const currentCart = state.cart;
                //check if item is already in the cart or not in the cart
                const indexToFetch = currentCart.findIndex((e)=>{
                    return e.id === action.payload.id;
                });
                if(indexToFetch==-1){
                    //if item is not in the cart
                    return {cart: state.cart.concat(action.payload)};
                }else{
                    return {cart: state.cart}
                }
                
            }

        case UPDATE_CART:
            const currentCartToUpdate = state.cart;
            const indexToUpdate = currentCartToUpdate.findIndex((e)=>{
                return e.id === action.payload.id;
            });
            currentCartToUpdate[indexToUpdate].qty+=action.payload.unit;
            const qty= currentCartToUpdate[indexToUpdate].qty;
            const firstPart = state.cart.slice(0, indexToUpdate);
            const lastPart = state.cart.slice(indexToUpdate+1);
            if(qty<=0){
                return {cart: firstPart.concat(lastPart)};
            }else{
                return {cart: firstPart.concat(currentCartToUpdate[indexToUpdate]).concat(lastPart)};
            }
        
        case REMOVE_FROM_CART:
            const cuurentCartToRemove = state.cart;
            const indexToRemove = cuurentCartToRemove.findIndex((e)=>{
                return e.id === action.payload;
            });
            const prevCartItems = state.cart.slice(0, indexToRemove);
            const nextCartItems = state.cart.slice(indexToRemove+1);
            return { cart: prevCartItems.concat(nextCartItems) }

        default:
            return state;
    }
}