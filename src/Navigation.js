import React from 'react';
import { Router, Scene, Actions } from 'react-native-router-flux';
import { StyleSheet, TouchableHighlight, ScrollView, View, Text} from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { getFishList } from './actions/FishActions';
import { Constants } from 'expo';

import Main from './Main';
import Cart from './components/Cart';
import Product from './components/Product';
import Footer from './components/Footer';

class Navigation extends React.Component{
    componentWillMount(){
        if(this.props.fishList.length>0){
            return;
        }else{
            this.props.getFishList();
        }
    }
    render(){
        return (
            <View>
                <Router navigationBarStyle={{ backgroundColor: '#81b71a' }}>
                    <Scene key="root">
                        <Scene key="home" component={Main} title="HoME COMASD"/>
                        <Scene key="cartHome" component={Cart} title="Fish UP"/>
                        <Scene key="product" component={Product} title="Product"/>
                    </Scene>
                </Router>
                <TouchableHighlight onPress={()=>Actions.cartHome()} style={styles.v1}>
                    <Text>
                        Click
                    </Text>
                </TouchableHighlight>
                <Footer
                    totalItems={this.props.cart.length}
                 />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    r1:{
        marginTop:Constants.statusBarHeight
    },
    v1: {
        marginTop: 100
    }
})

function mapStateToProps(state){
    return {
        fishList: state.fishList.fishList,
        currentScreen: state.currentScreen.currentScreen,
        cart: state.cart.cart
    };
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({
        getFishList
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Navigation);
