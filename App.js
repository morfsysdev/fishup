import React from 'react';
import { AppRegistry, View, Text, StyleSheet } from 'react-native';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import reduxThunk from 'redux-thunk';

import reducers from './src/reducers';
import Main from './src/Main';
//import Navigation from './src/Navigation';

const store = createStore(reducers, applyMiddleware(reduxThunk));

export default class App extends React.Component{
  render(){
    return (
      <Provider store={store}>
        <Main />
      </Provider>
    )
  }
}

AppRegistry.registerComponent('thep', ()=>App);
